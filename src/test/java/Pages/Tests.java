package Pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import sun.security.provider.VerificationProvider;

import static org.junit.Assert.assertEquals;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;


public class Tests {
    WebSite webSite;
    WebDriverWait wait;
    Actions builder;

    String correctEmail = "raindropslol@mail.ru";
    String correctPass = "winda7";
    String incorrectEmail = "qwe@mail.ru";
    String incorrectPass = "qwe";

    private WebDriver webDriver;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
//        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void preCondition(){
        webDriver = new ChromeDriver();
//        webDriver = new FirefoxDriver();

//        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("headless");
//        chromeOptions.addArguments("window-size=1200x600");
//        webDriver = new ChromeDriver(chromeOptions);

        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 90, 300);
        webDriver.manage().deleteAllCookies();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(90, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS);
        webDriver.get("https://app.heroicnow.com/");
//        webDriver.get("http://the-internet.herokuapp.com/drag_and_drop");
    }
    /*********** LOGIN ***********/



    @Test
    public void emptyEmailField (){
       webSite.loginPage().EmailField.clear();
       webSite.loginPage().SignInButton.click();
        Assert.assertTrue(webSite.loginPage().AllertField.getText().contains("This field may not be blank."));
    }


    @Test
    public void emptyPassField(){
        webSite.loginPage().EmailField.sendKeys("qwe@mail.ru");
        webSite.loginPage().PasswordField.clear();
        webSite.loginPage().SignInButton.click();
        Assert.assertTrue(webSite.loginPage().AllertField.getText().contains("This field may not be blank."));
    }

    @Test
    public void incorrectEmailField(){
        webSite.loginPage().EmailField.sendKeys(incorrectEmail);
        webSite.loginPage().PasswordField.sendKeys(incorrectPass);
        webSite.loginPage().SignInButton.click();
        Assert.assertTrue(webSite.loginPage().AllertField.getText().contains("Oops. Incorrect email"));
    }
    @Test
    public void correctEmail_incorrectPass(){
        webSite.loginPage().EmailField.sendKeys(correctEmail);
        webSite.loginPage().PasswordField.sendKeys(incorrectPass);
        webSite.loginPage().SignInButton.click();
        Assert.assertTrue(webSite.loginPage().AllertField.getText().contains("Oops. Incorrect password"));
    }
    @Test
    public void restoringPassword_incorrectEmail(){
         webSite.loginPage().RestoringPassword.click();
         wait.until(ExpectedConditions.elementToBeClickable(webSite.loginPage().RestPassButton));
         webSite.loginPage().RestPassEmailField.sendKeys(incorrectPass);
         webSite.loginPage().RestPassButton.click();
         Assert.assertTrue(webSite.loginPage().RestPassAllerField.getText().contains("Sorry - we don't have any accounts for this email address. Try another?"));
         webSite.loginPage().BackToLogin.click();
    }
//
    @Test
    public void correctValue() {
        webSite.loginPage().EmailField.sendKeys(correctEmail);
        webSite.loginPage().PasswordField.sendKeys(correctPass);
        webSite.loginPage().SignInButton.click();
        wait.until(ExpectedConditions.visibilityOf(webSite.mainPage().AddNewPage));
        Assert.assertTrue(webDriver.findElement(By.id("logger")).isDisplayed());
    }


    // SIDEBAR
    @Test
    public void SettingPageLinks() {
        webSite.supportMethods().signIn();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("logger")));
        webSite.mainPage().SideBarSettings.click();
        List<WebElement> list = new ArrayList<>();
        list = webDriver.findElements(By.xpath("//*[@class='heroSidebarMenu']//li[@class=' pageMenu__list-item']"));
        for(int i = 2; i < list.size(); i++){
            wait.until(ExpectedConditions.elementToBeClickable(list.get(i))).click();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().BackArrow)).click();
        }
        Assert.assertTrue(webSite.mainPage().SideBarSettings.isDisplayed());
    }
    @Test
    public void prebuiltsLinks() throws InterruptedException {
        webSite.supportMethods().signIn();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("logger")));
        webSite.supportMethods().cleaner();
        webSite.sideBar().Prebuilts.click();
        List<WebElement> list = new ArrayList<>();
        list = webDriver.findElements(By.xpath("//*[@class='heroSidebar']//li[@class=' pageMenu__list-item']"));
        for(int i = 2; i < list.size();i++){
            wait.until(ExpectedConditions.elementToBeClickable(list.get(i))).click();
        }
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[@class='sidebar-inner']//button[@class='hero-btn-add-new']")).isDisplayed());
    }

    @Test
    public void sidebarPrebuiltsLinks() throws InterruptedException {
        webSite.supportMethods().signIn();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("logger")));
        webSite.supportMethods().cleaner();
        webSite.sideBar().Prebuilts.click();
        List<WebElement> list = new ArrayList<>();
        list.add(webSite.sideBar().NavigationHeaders);
        list.add(webSite.sideBar().Banners);
        list.add(webSite.sideBar().Text);
        list.add(webSite.sideBar().TextAndImage);
        list.add(webSite.sideBar().CTA);
        list.add(webSite.sideBar().Testimonais);
        list.add(webSite.sideBar().Blog);
        list.add(webSite.sideBar().Footer);
        list.add(webSite.sideBar().MyCustomPrebuilts);
        for(int i = 0; i < list.size();i++){
            wait.until(ExpectedConditions.elementToBeClickable(list.get(i))).click();
        }
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[@class='sidebar-inner']//button[@class='hero-btn-add-new']")).isDisplayed());
    }
    @Test
    public void SidebarSettingsinsideLinks() throws InterruptedException {
        List<WebElement> list = new ArrayList<>();
        List<WebElement> elements = new ArrayList<>();
        webSite.supportMethods().signIn();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("logger")));
        webSite.supportMethods().cleaner();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Settings)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().PageSettings)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Page)).click();
        wait.until(ExpectedConditions.visibilityOf(webSite.sideBar().AssertForPageSettings));
        Assert.assertTrue(webSite.sideBar().AssertForPageSettings.isDisplayed());

        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Back)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().BackArrow)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Style)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Fonts)).click();
        elements = webDriver.findElements(By.xpath("//div[@class='accordionPanel mainAccordion']"));
        Assert.assertTrue(elements.size() >8);

        list = webDriver.findElements(By.xpath("//i[@class='icon-left-open']"));
        wait.until(ExpectedConditions.elementToBeClickable(list.get(1))).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().ButtondAndFields)).click();
        elements = webDriver.findElements(By.xpath("//div[@class='accordionPanel mainAccordion']"));
        Assert.assertTrue(elements.size()>5);

        list = webDriver.findElements(By.xpath("//i[@class='icon-left-open']"));
        wait.until(ExpectedConditions.elementToBeClickable(list.get(1))).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Colors)).click();
        elements = webDriver.findElements(By.xpath("//div[@class='accordionPanelHeader']"));
        Assert.assertTrue(elements.size() > 1);

        list = webDriver.findElements(By.xpath("//i[@class='icon-left-open']"));
        wait.until(ExpectedConditions.elementToBeClickable(list.get(1))).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().CustomCSS)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().AssertForCustomCSS));
        Assert.assertTrue(webSite.sideBar().AssertForCustomCSS.isDisplayed());

        list = webDriver.findElements(By.xpath("//i[@class='icon-left-open']"));
        wait.until(ExpectedConditions.elementToBeClickable(list.get(1))).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().BackArrow)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().SocialAccounts)).click();
        elements = webDriver.findElements(By.xpath("//div[@class='fieldLabelElement']"));
        Assert.assertTrue(elements.size() > 5);

        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().BackArrow)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Integrations)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().AssertForIntegrations));
        Assert.assertTrue(webSite.sideBar().AssertForIntegrations.isDisplayed());

        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().BackArrow)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().Domains)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().NewDomains)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().AssertForDomains));
        Assert.assertTrue(webSite.sideBar().AssertForDomains.isDisplayed());

        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().BackArrow)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().BackArrow)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().MyAccountDetals)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().AssertForMyAccountDetals));
        Assert.assertTrue(webSite.sideBar().AssertForMyAccountDetals.isDisplayed());
    }


    @After
    public void postCondition() {
        if(webDriver != null)
            webDriver.quit();
    }

}
