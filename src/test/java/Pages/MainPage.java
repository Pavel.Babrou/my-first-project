package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class MainPage {
    private WebDriver webDriver;

    public MainPage(WebDriver driver){
        webDriver = driver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy (xpath = "//div[@class='no-change-item font-size-element-style']")
    WebElement TextIntoParagraph;
    @FindBy (xpath = "//div[contains(@class,'column-col')]")
    WebElement Column;
    @FindBy (xpath = "//div[@class='content-sections']//div[contains(@id,'section')]")
    WebElement AllSections;
    @FindBy (xpath = "//div[@class='empty-section add-new-section']//button[@class='button-section-panel'][text() = 'Create Custom Section +']")
    WebElement CreateCustomSection;
    @FindBy (xpath = "//div[@class='empty-section add-new-section']//button[@class='button-section-panel'][text() = 'Insert Pre Built module']")
    WebElement CreatePrebuiltsSection;
    @FindBy (xpath = "//textarea[contains(@id,'tmp_text')]")
    WebElement TextAreaSection;
    @FindBy (xpath = "//div[@class='root-block-tinymce-editor__cover active']")
    WebElement ParagraphSection;

    /*********** ЭЛЕМЕНТЫ ДЛЯ КАСТОМНЫХ СЕКЦИЙ ***********/

    @FindBy (xpath = "//div[@class='content-sections']//button[contains(@id,'plus')]")
    WebElement PlusInsideSection;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Button')]")
    WebElement ElementButton;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Header')]")
    WebElement ElementHeader;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Paragraph')]")
    WebElement ElementParagraph;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Image')]")
    WebElement ElementImage;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Video')]")
    WebElement ElementVideo;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Divider')]")
    WebElement ElementDivider;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Map')]")
    WebElement ElementMap;

    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Input')]")
    WebElement ElementInput;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Text Area')]")
    WebElement ElementTextArea;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Droplist')]")
    WebElement ElementDroplist;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Checkbox')]")
    WebElement ElementCheckbox;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Radiobox')]")
    WebElement ElementRadiobox;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Submit Button')]")
    WebElement ElementSubmitButton;

    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Timer')]")
    WebElement ElementTimer;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Social')]")
    WebElement ElementSocial;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='HTML/js')]")
    WebElement ElementHTML;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Comments')]")
    WebElement ElementComments;
    @FindBy (xpath = "//div[@class='modal-add-element__items']//div[@class='custom-edit-element__title'][(text() ='Navigation')]")
    WebElement ElementNavigation;

    /*********** PREBUILT MODULES ***********/
//NAVIGATION HEADERS
    @FindBy (xpath = "//html//div[@id='category-nav']//div[1]/div[3]/div[1]/button[2]")
    WebElement SimpleLeft;
    @FindBy (xpath = "//html//div[@id='category-nav']//div[2]/div[3]/div[1]/button[2]")
    WebElement SimpleStacked;
    @FindBy (xpath = "//html//div[@id='category-nav']//div[3]/div[3]/div[1]/button[2]")
    WebElement SimpleRight;
    @FindBy (xpath = "//html//div[@id='category-nav']//div[4]/div[3]/div[1]/button[2]")
    WebElement SimpleCenter;
    @FindBy (xpath = "//html//div[@id='category-nav']//div[5]/div[3]/div[1]/button[2]")
    WebElement CTAStacked;
//BANNERS
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[1]/div[3]/div/button[2]")
    WebElement Banner1;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[2]/div[3]/div/button[2]")
    WebElement Banner2;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[3]/div[3]/div/button[2]")
    WebElement Banner3;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[4]/div[3]/div/button[2]")
    WebElement Banner4;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[5]/div[3]/div/button[2]")
    WebElement Banner5;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[6]/div[3]/div/button[2]")
    WebElement Banner6;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[7]/div[3]/div/button[2]")
    WebElement Banner7;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[8]/div[3]/div/button[2]")
    WebElement Banner8;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[9]/div[3]/div/button[2]")
    WebElement Banner9;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[10]/div[3]/div/button[2]")
    WebElement Banner10;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[11]/div[3]/div/button[2]")
    WebElement Banner11;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[12]/div[3]/div/button[2]")
    WebElement Banner12;
    @FindBy (xpath = "//div[contains(@class,'column-col')]")
    WebElement ColumnColor;
    @FindBy (xpath = "//li[contains(@id,'navItem')]")
    WebElement NavFontColor;
    @FindBy (xpath = "//div[contains(@class,'container-fluid')]")
    WebElement SectionColor;






    @FindBy (xpath = "//div[@class='sidebar-inner']//button[@class='hero-btn-add-new']")
    WebElement CreateBlankPage;
    @FindBy (xpath = "//div[@id='coll-ful54834803']//div[@class='column-col  bg-center-fit  cornersAll  column-edit-section']")
    WebElement Logo;
    @FindBy (xpath = "//div[contains(@class,'container-fluid edit-container')]")
    WebElement Section;
    @FindBy (xpath = "//*[@id=\"category-36\"]/div[2]/div[8]/div[2]/div/img")
    WebElement Canvas;
    @FindBy (xpath = "//*[.='Blank Canvas']/following-sibling::*//button[.='Add to Pages']")
    WebElement BlankCanvas;
    @FindBy (xpath = "//li[@data-panel-type='moveDown']")
    WebElement MoveDown;
    @FindBy (xpath = "//li[@data-panel-type='moveUp']")
    WebElement MoveUp;
    @FindBy (xpath = "//span[@class='edit__name'][contains(text(),'Edit')]")
    WebElement Edit;
    @FindBy (xpath = "//i[@class='icon-sidebar-menu-settings']")
    WebElement SideBarSettings;
    @FindBy (xpath = "//a[@href='#'][contains(text(),'Page settings')]")
    WebElement PageSettings;
    @FindBy (xpath = "//i[@class='icon-left-open']")
    WebElement BackArrow;
    @FindBy (xpath = "//a[@href='#'][contains(text(),'new home page')]")
    WebElement NewHomePage;
    @FindBy (xpath = "//div[@class='content-sections']//div[contains(@class,'section-index-0')]")
    WebElement ClickBySection;
    @FindBy (xpath = "//li[@data-panel-type='delete']")
    WebElement Delete;
    @FindBy (xpath = "//button[@class='delete']")
    WebElement DeleteButton;
    @FindBy (xpath = "//div[@id='category-37']//button[@class='page-image-item__action']")
    WebElement AddFirstPage;
    @FindBy (xpath = "//i[@class='icon-sidebar-menu-sections']")
    WebElement Sections;
    @FindBy (xpath = "//*[@id=\"root\"]/div/aside/aside/div[1]/div/div[1]/button/i")
    WebElement PagesAndPosts;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'Full Width Column')]")
    WebElement Colum1;
    @FindBy (xpath = "//i[contains(@class,'addSectionPanel')]")
    WebElement Plus;
    @FindBy (xpath = "//button[contains(text(),'Add Custom Section')]")
    WebElement AddCustom;
    @FindBy (xpath = "//button[contains(text(),'Add Prebuilt')]")
    WebElement AddPrebuilt;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'80% Width Column')]")
    WebElement Colum2;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'60% Width Column')]")
    WebElement Colum3;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'30% Width Column')]")
    WebElement Colum4;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'Full Width Symmetrical')]")
    WebElement Colum5;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'90% Width 2 Column')]")
    WebElement Colum6;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'80% Width 2 Column')]")
    WebElement Colum7;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'60% Width 2 Column')]")
    WebElement Colum8;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'Right Sidebar Wide')]")
    WebElement Colum9;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'Right Sidebar Narrow')]")
    WebElement Colum10;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'Left Sidebar Wide')]")
    WebElement Colum11;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'Left Sidebar Narrow')]")
    WebElement Colum12;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'3 Column Row')]")
    WebElement Colum13;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'4 Columns')]")
    WebElement Colum14;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'6 Columns')]")
    WebElement Colum15;
    @FindBy (xpath = "//div[@class='column-grid__title'][contains(text(),'8 Columns')]")
    WebElement Colum16;
    @FindBy (xpath = "//html//div[@id='category-36']//div[1]/div[3]/div[1]/button[2]")
    WebElement AddEmptyPage;
    @FindBy (xpath = "//div[@class='animationSidebar']//div[@class='pageMenu__item']")
    WebElement Page;
    @FindBy (xpath = "//*[@id=\"root\"]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/ul/div[1]/li/div/div/a/i")
    WebElement ThreeDots;
    @FindBy (xpath = "//*[@id=\"root\"]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/ul/div[2]/li/div/div/a/i")
    WebElement ThreeDots2;
    @FindBy (xpath = "/html/body/div[4]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/ul/div/li/div/div/div/div[2]/ul/li[1]/a")
    WebElement DeleteElements;
    @FindBy (xpath = "//*[@id=\"root\"]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/div[2]/button")
    WebElement AddNewPage;
    @FindBy (xpath = "//*[@id=\"category-banner\"]/div[2]/div[2]/div[2]/div/img")
    WebElement AddBanner;





}
