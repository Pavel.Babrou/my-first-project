package Pages;

import org.openqa.selenium.WebDriver;

public class WebSite {
        WebDriver webDriver;

    public WebSite (WebDriver driver){
            webDriver = driver;
        }
    public LoginPage loginPage (){
            return new LoginPage(webDriver);
    }
    public StartPage startPage(){
            return new StartPage(webDriver);
    }
    public RegistrationPage registrationPage(){
            return new RegistrationPage(webDriver);
    }
    public MainPage mainPage(){
        return new MainPage(webDriver);
    }
    public SideBar sideBar(){
        return new SideBar((webDriver));
    }
    public SupportMethods supportMethods(){
        return new SupportMethods(webDriver);
    }
}
