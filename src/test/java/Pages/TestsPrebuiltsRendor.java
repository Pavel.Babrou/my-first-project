package Pages;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.Theories;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestsPrebuiltsRendor {
    WebDriver webDriver;
    WebSite webSite;
    WebDriverWait wait;
    Actions builder;

    @Before
    public void preCondition(){
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\ChromeDriver\\chromedriver.exe");
        webDriver = new ChromeDriver();
//        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("headless");
//        chromeOptions.addArguments("window-size=1200x600");
//        webDriver = new ChromeDriver(chromeOptions);

        webSite = new WebSite(webDriver);;
        wait = new WebDriverWait(webDriver, 45, 300);
        webDriver.manage().deleteAllCookies();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(45, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(45, TimeUnit.SECONDS);
        webDriver.get("https://app.heroicnow.com/");
    }

    @Test
    public void navigationHeaders() throws InterruptedException {
        builder = new Actions(webDriver);
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        List <WebElement> list = new ArrayList<>();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddNewPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateBlankPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreatePrebuiltsSection)).click();
        for(int i = 0; i < webSite.supportMethods().getHeaders().size();i++){
            wait.until(ExpectedConditions.elementToBeClickable(webSite.supportMethods().getHeaders().get(i))).click();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ClickBySection));
            builder.moveToElement(webSite.mainPage().ClickBySection).perform();
            builder.moveToElement(webSite.mainPage().ClickBySection).click().perform();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Plus)).click();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddPrebuilt)).click();
        }
        Thread.sleep(4000);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().PagesAndPosts)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ThreeDots));
        list = webDriver.findElements(By.xpath("//*[@id=\"root\"]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/ul/div/li/div/div/a/i"));
        list.get(0).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.sideBar().PreviewPage)).click();
        list = webDriver.findElements(By.xpath("//nav[contains(@id,'tmp_nav')]"));
        Assert.assertTrue(list.size()>=5);
    }

    @Test
    public void banners() throws InterruptedException {
        builder = new Actions(webDriver);
        webSite.supportMethods().signIn();
        webSite.supportMethods().cleaner();
        List<WebElement> list = new ArrayList<>();

        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddNewPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreateBlankPage)).click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().CreatePrebuiltsSection)).click();
        builder.moveToElement(webSite.mainPage().Banner1).perform();
        builder.moveToElement(webSite.mainPage().Banner1).click().perform();
        for( int i = 0; i < webSite.supportMethods().getBaners().size();i++){
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@id,'smart-section')]")));
            webDriver.findElement(By.xpath("//div[contains(@id,'smart-section')]")).click();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Plus)).click();
            wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().AddPrebuilt)).click();
            builder.moveToElement(webSite.supportMethods().getBaners().get(i)).perform();
            builder.moveToElement(webSite.supportMethods().getBaners().get(i)).click().perform();
        }
        Thread.sleep(4000);
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().Sections)).click();
        webSite.mainPage().PagesAndPosts.click();
        wait.until(ExpectedConditions.elementToBeClickable(webSite.mainPage().ThreeDots));
        list = webDriver.findElements(By.xpath("//*[@id=\"root\"]/div/aside/aside/div[2]/div[2]/div/div/div[1]/div/nav/div[1]/ul/div/li/div/div/a/i"));
        list.get(0).click();
        webSite.sideBar().PreviewPage.click();
        Assert.assertTrue(webDriver.findElement(By.xpath("//div[contains(@id,'smart-section')]")).isDisplayed());
    }


    @After
    public void postCondition() throws InterruptedException {
        if(webDriver != null)
            webDriver.quit();
    }
}
